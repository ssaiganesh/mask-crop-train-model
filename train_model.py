import os
import time
import copy
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import datasets, transforms
from torch.utils.data import DataLoader
from Mask_Classifier import Classifier
from train_model import split_data

split_data('./input','./output',0.8)

data_transforms = {
    'train':
        transforms.Compose([
            transforms.RandomResizedCrop(size=36, scale=(0.8, 1.0)),
            transforms.RandomRotation(degrees=20),
            transforms.ColorJitter(),
            transforms.RandomHorizontalFlip(),
            transforms.CenterCrop(size=32),
            transforms.ToTensor(),
            # transforms.Normalize([0.578, 0.468, 0.429],
            #                      [0.257, 0.243, 0.246])
            #transforms.Normalize([0.574, 0.462, 0.421],
            #                    [0.259, 0.244, 0.246])
            transforms.Normalize([0.485, 0.456, 0.406],
                                 [0.229, 0.224, 0.225])  # Imagenet standards
        ]),
    'val': transforms.Compose([
        transforms.Resize(36),
        transforms.CenterCrop(32),
        transforms.ToTensor(),
        # transforms.Normalize([0.578, 0.468, 0.429],
        #                     [0.257, 0.243, 0.246])
        #transforms.Normalize([0.574, 0.462, 0.421],
        #                        [0.259, 0.244, 0.246])
        transforms.Normalize([0.485, 0.456, 0.406],
                                 [0.229, 0.224, 0.225])  # Imagenet standards
    ]),
}
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
data_dir = './output'
datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x), data_transforms[x])
                  for x in ['train', 'val']}
dataloaders = {x: DataLoader(datasets[x], batch_size=6, shuffle=True, num_workers=4)
               for x in ['train', 'val']}
dataset_sizes = {x: len(datasets[x]) for x in ['train', 'val']}
class_names = datasets['train'].classes

# initialize model
model = Classifier(input_size=(3, 32, 32), output_class=len(class_names))
model.to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
scheduler = lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)
num_epochs = 25

since = time.time()
best_model_wts = copy.deepcopy(model.state_dict())
best_acc = 0.0

model.train()
for epoch in range(num_epochs):
    print('Epoch {}/{}'.format(epoch+1, num_epochs))
    print('-' * 10)

    running_loss, running_loss_val = 0.0, 0.0
    running_corrects, running_corrects_val = 0, 0

    # Iterate over data.
    for inputs, labels in dataloaders['train']:
        inputs = inputs.to(device)
        labels = labels.to(device)

        # zero the parameter gradients
        optimizer.zero_grad()

        outputs = model(inputs)
        _, preds = torch.max(outputs, 1)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # statistics
        running_loss += loss.item() * inputs.size(0)
        running_corrects += torch.sum(preds == labels.data)

    scheduler.step()
    epoch_loss = running_loss / dataset_sizes['train']
    epoch_acc = running_corrects.double() / dataset_sizes['train']
    print('{} Loss: {:.4f} Acc: {:.4f}'.format('train', epoch_loss, epoch_acc))

    model.eval()
    for inputs_val, labels_val in dataloaders['val']:
        inputs_val = inputs_val.to(device)
        labels_val = labels_val.to(device)
        with torch.no_grad():
            outputs_val = model(inputs_val)
            _, preds_val = torch.max(outputs_val, 1)
            loss_val = criterion(outputs_val, labels_val)

        # statistics
        running_loss_val += loss_val.item() * inputs_val.size(0)
        running_corrects_val += torch.sum(preds_val == labels_val.data)
    epoch_loss_val = running_loss_val / dataset_sizes['val']
    epoch_acc_val = running_corrects_val.double() / dataset_sizes['val']
    print('{} Loss: {:.4f} Acc: {:.4f}'.format('val', epoch_loss_val, epoch_acc_val))
    model.train()

    # deep copy the model
    if epoch_acc_val > best_acc:
        best_acc = epoch_acc_val
        best_model_wts = copy.deepcopy(model.state_dict())

time_elapsed = time.time() - since
print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
print('Best val Acc: {:4f}'.format(best_acc))

# load best model weights
model.load_state_dict(best_model_wts)
torch.save(model.state_dict(), 'mask_classifier_model.pth')
