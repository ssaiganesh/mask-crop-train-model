from torch.nn import Linear, ReLU, Sequential, Conv2d, MaxPool2d, Module, Softmax, BatchNorm2d, \
    Dropout, BatchNorm1d
import numpy as np


class Classifier(Module):
    def __init__(self, input_size=(3, 32, 32), output_class=2):
        super(Classifier, self).__init__()
        self.cnn_layers = Sequential(
            # Defining a 2D convolution layer
            # taking input size 32 as an example: 3*32*32 --> 16*32*32
            Conv2d(3, 16, kernel_size=3, stride=1, padding=1),
            ReLU(inplace=True),
            BatchNorm2d(16),
            # taking input size 32 as an example: 16*32*32 --> 16*32*32
            Conv2d(16, 16, kernel_size=3, stride=1, padding=1),
            ReLU(inplace=True),
            BatchNorm2d(16),
            # taking input size 32 as an example: 16*16*16 --> 16*16*16
            MaxPool2d(kernel_size=2, stride=2),
            Dropout(p=0.25),

            # Defining another 2D convolution layer
            # taking input size 32 as an example: 16*16*16 --> 32*16*16
            Conv2d(16, 32, kernel_size=3, stride=1, padding=1),
            ReLU(inplace=True),
            BatchNorm2d(32),
            # taking input size 32 as an example: 32*16*16 --> 32*16*16
            Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
            ReLU(inplace=True),
            BatchNorm2d(32),
            # taking input size 32 as an example: 32*16*16 --> 32*8*8
            MaxPool2d(kernel_size=2, stride=2),
            Dropout(0.5)
        )
        n_size = int(np.floor(32*(input_size[1]/4)**2))
        self.linear_layers = Sequential(
            Linear(n_size, 64),
            ReLU(inplace=True),
            BatchNorm1d(64),
            Dropout(0.5),
            Linear(64, output_class),
            Softmax()
        )

    def forward(self, x):
        x = self.cnn_layers(x)
        x = x.view(x.size(0), -1)
        x = self.linear_layers(x)
        return x
