import os
from PIL import Image 
import cv2

### for text file
def lists_txt(txt_dir):

    a_file = open(txt_dir, "r")
    list_of_lists = [(line.strip()).split() for line in a_file]
    a_file.close()
    return list_of_lists


def crop_picture(cwd, im, dh, dw, coordinates, name):
    # Setting the points for cropped image
    i = 0 
    for i in range(len(coordinates)):
        #mask or no mask 
        zero_one = int(coordinates[i][0])
        x = float(coordinates[i][1])
        y = float(coordinates[i][2])
        w = float(coordinates[i][3]) 
        h = float(coordinates[i][4])
        
        l = (x - w / 2) * dw
        r = (x + w / 2) * dw
        t = (y - h / 2) * dh
        b = (y + h / 2) * dh
        im1 = im.crop((l, t, r, b))
        im1.save(f'{cwd}/{zero_one}/{name}_{i}.jpg')


    return f'saved image {i}.jpg'

def main():
    cwd = os.getcwd()
    os.mkdir(f'{cwd}/input')
    os.mkdir(f'{cwd}/input/0')
    os.mkdir(f'{cwd}/input/1')
    annotations = cwd+'/Annotations'
    txt_files = [f for f in os.listdir(annotations) if f.endswith('.txt')]
    for i in txt_files: 
        name = i.split('.')[0]
        image_name = name + '.jpg'
        coordinates = lists_txt(annotations + '/' + i)
        img_dir = annotations + '/' + image_name
        img = cv2.imread(img_dir)
        dh, dw = img.shape[:2]
        im = Image.open(img_dir)
        crop_picture(cwd,im, dh,dw,coordinates, name)




if __name__ == '__main__':
    main()
    
